---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1080 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@manojmj](https://gitlab.com/manojmj) | 3 | 560 |
| [@pks-t](https://gitlab.com/pks-t) | 4 | 500 |
| [@alexpooley](https://gitlab.com/alexpooley) | 5 | 500 |
| [@whaber](https://gitlab.com/whaber) | 6 | 400 |
| [@theoretick](https://gitlab.com/theoretick) | 7 | 400 |
| [@10io](https://gitlab.com/10io) | 8 | 380 |
| [@mksionek](https://gitlab.com/mksionek) | 9 | 340 |
| [@mrincon](https://gitlab.com/mrincon) | 10 | 340 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 11 | 300 |
| [@sabrams](https://gitlab.com/sabrams) | 12 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 13 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 14 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 15 | 250 |
| [@mattkasa](https://gitlab.com/mattkasa) | 16 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 17 | 200 |
| [@leipert](https://gitlab.com/leipert) | 18 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 19 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 20 | 200 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 21 | 170 |
| [@jerasmus](https://gitlab.com/jerasmus) | 22 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 23 | 140 |
| [@twk3](https://gitlab.com/twk3) | 24 | 130 |
| [@balasankarc](https://gitlab.com/balasankarc) | 25 | 110 |
| [@cablett](https://gitlab.com/cablett) | 26 | 100 |
| [@stanhu](https://gitlab.com/stanhu) | 27 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 28 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 29 | 100 |
| [@splattael](https://gitlab.com/splattael) | 30 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 31 | 80 |
| [@mkozono](https://gitlab.com/mkozono) | 32 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 33 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 34 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 35 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 36 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 37 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 38 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 39 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 40 | 80 |
| [@ahegyi](https://gitlab.com/ahegyi) | 41 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 42 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 43 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 44 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 45 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 46 | 50 |
| [@mwoolf](https://gitlab.com/mwoolf) | 47 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 48 | 40 |
| [@tkuah](https://gitlab.com/tkuah) | 49 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 50 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 51 | 40 |
| [@proglottis](https://gitlab.com/proglottis) | 52 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 53 | 30 |
| [@cngo](https://gitlab.com/cngo) | 54 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 55 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |
| [@rspeicher](https://gitlab.com/rspeicher) | 8 | 30 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |
| [@tnir](https://gitlab.com/tnir) | 2 | 200 |

## FY22-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@pks-t](https://gitlab.com/pks-t) | 1 | 500 |
| [@manojmj](https://gitlab.com/manojmj) | 2 | 500 |
| [@10io](https://gitlab.com/10io) | 3 | 380 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 4 | 300 |
| [@mrincon](https://gitlab.com/mrincon) | 5 | 300 |
| [@mksionek](https://gitlab.com/mksionek) | 6 | 200 |
| [@mkozono](https://gitlab.com/mkozono) | 7 | 80 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 8 | 80 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 9 | 60 |
| [@tkuah](https://gitlab.com/tkuah) | 10 | 40 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rspeicher](https://gitlab.com/rspeicher) | 1 | 30 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@tnir](https://gitlab.com/tnir) | 1 | 200 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1000 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 4 | 400 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 6 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 8 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 250 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 10 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@leipert](https://gitlab.com/leipert) | 13 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 14 | 200 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 15 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@jerasmus](https://gitlab.com/jerasmus) | 17 | 140 |
| [@twk3](https://gitlab.com/twk3) | 18 | 130 |
| [@balasankarc](https://gitlab.com/balasankarc) | 19 | 110 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 20 | 110 |
| [@cablett](https://gitlab.com/cablett) | 21 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 22 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 23 | 100 |
| [@stanhu](https://gitlab.com/stanhu) | 24 | 100 |
| [@vsizov](https://gitlab.com/vsizov) | 25 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 26 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@splattael](https://gitlab.com/splattael) | 29 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 30 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 31 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 32 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 33 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 34 | 80 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 35 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 38 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@ck3g](https://gitlab.com/ck3g) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@cngo](https://gitlab.com/cngo) | 47 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


